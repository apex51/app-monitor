#!/bin/bash

# init working dir
bin_dir=`dirname $0`
log_dir="$(dirname $bin_dir)/log"

# cron check
bash $bin_dir/user_action.sh check >> $log_dir/user_action.cron_log
bash $bin_dir/user_info.sh check >> $log_dir/user_info.cron_log
bash $bin_dir/web_pv.sh check >> $log_dir/web_pv.cron_log
bash $bin_dir/app_server.sh check >> $log_dir/app_server.cron_log
bash $bin_dir/app_da.sh check >> $log_dir/app_da.cron_log
bash $bin_dir/pi_batch.sh check >> $log_dir/pi_batch.cron_log
bash $bin_dir/doctor_index.sh check >> $log_dir/doctor_index.cron_log
bash $bin_dir/user_keywords.sh check >> $log_dir/user_keywords.cron_log