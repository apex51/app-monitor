#!/bin/bash

if [ $# -lt 1 ]; then
  echo "usage: $0 (start|stop|check|batch)"
  exit 1
fi

# config env
export SPARK_HOME=/opt/cloudera/parcels/CDH/lib/spark
export PATH=$SPARK_HOME/bin:$PATH
export JAVA_HOME=/opt/java8
export PATH=/opt/java8/bin:$PATH

# init variables
email_from="uchadoop@dxy.cn"  
email_to="jiangh@dxy.cn"
email_subject="PI: APP DA Fast Parse Failed"
email_content="APP DA job is not running, trying to reboot yo ... "

# init working dir
bin_dir=`dirname $0`
jar_dir="$(dirname $bin_dir)/jar"
log_dir="$(dirname $bin_dir)/log"
dat_dir="$(dirname $bin_dir)/dat"

# files
external_files=$jar_dir/spark/api_rules_app_da_pv.yaml,$jar_dir/spark/mydata4vipday2.datx
jar_file=$jar_dir/AppDAFastStream-assembly-1.0.jar
pid_file=$dat_dir/app_da.pid
flag_file=$dat_dir/app_da.flag
log_file=$log_dir/app_da.log

# spark submit command
cmd='nohup spark-submit \
--class TaskStreaming \
--files $external_files \
--driver-memory 3G \
--executor-cores 1 \
--executor-memory 1G \
--num-executors 4 \
--conf spark.locality.wait=0 \
--conf spark.streaming.kafka.maxRatePerPartition=500 \
--conf spark.streaming.backpressure.initialRate=500 \
$jar_file app-da-log etl/checkpoint 1 PI-app_pv_user_log_test PI-app_ev_user_log_test > $log_file 2>&1 & echo $! > $pid_file'

# batch variables
pv_table=log.bdl_app_da_pv_di
ev_table=log.bdl_app_da_event_di
batch_log_file=$log_dir/app_da_batch.log
batch_pid_file=$dat_dir/app_da_batch.pid

case "$1" in
    start)
        echo "ok" > $flag_file
        eval "$cmd"
        ;;
    stop)
        echo "wait" > $flag_file
        echo "Kill pid $(cat $pid_file)"
        kill $(cat $pid_file)
        ;;
    check)
        # check if pid exists
        # if error, res would be empty
        res=`ps h -fp $(cat $pid_file)`
        flag=`cat $flag_file`
        if [ "$res" == "" ] && [ "$flag" == "ok" ]; then
            echo "no app da job running ..."
            /usr/bin/mail -s "$email_subject" $email_to < $log_file
            eval "$cmd"
        elif [ "$flag" == "wait" ]; then
            echo "wait for app da"
        else
            echo "app da is running"
        fi
        ;;
    clean)
        echo "cleaning hdfs checkpoint dir ..."
        hadoop fs -rmr etl/checkpoint/app_da
        ;;
    batch)
        echo "launch batch job for app da"
        if [ $# -eq 4 ]; then
            nohup spark-submit --class TaskBatch --num-executors 10 --files $external_files $jar_file $pv_table $ev_table $2 $3 $4 > $batch_log_file 2>&1 & echo $! > $batch_pid_file
        elif [ $# -eq 5 ]; then
            nohup spark-submit --class TaskBatch --num-executors 10 --files $external_files $jar_file $pv_table $ev_table $2 $3 $4 $5 > $batch_log_file 2>&1 & echo $! > $batch_pid_file
        else
            echo "usage: $0 batch date1 date2(optional)"
            exit 1
        fi
        ;;
    *)
        echo "usage: $0 (start|stop|check|batch)"
        exit 1
esac
