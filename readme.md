### 目录结构：

 - bin: 存放所有执行的脚本
 - jar: jar 包和需要上传的配置文件
 - log: 程序运行的日志和 cron 的日志
 - dat: 程序运行的 pid、中间状态

### crontab 的配置：

*/5 * * * * bash ~/monitor/bin/monitor.sh >> ~/monitor/log/monitor.cron_log

### 各种流式处理的状态 hdfs 保存位置

- fast-streams (web-pv, app-da, app-server): etl/checkpoint
- user-info, user-action: pi/checkpoint, pi/spark-data
- pi-batch: pi/batch/checkpoint, pi/batch/spark-data

### 流式处理启动过程：

1. （可选）删除掉 hdfs 上的断点保存数据
2. 三个 fast-stream 启动，两个 pi-user 程序启动

### 批量处理启动过程：

1. pi-batch clean 清理好环境
2. pi-batch batch date date 提交批量数据