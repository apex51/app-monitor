#!/bin/bash

if [ $# -lt 1 ]; then
  echo "usage: $0 (start|stop|clean|check)"
  exit 1
fi

# init working dir
bin_dir=`dirname $0`
jar_dir="$(dirname $bin_dir)/jar"
log_dir="$(dirname $bin_dir)/log"
dat_dir="$(dirname $bin_dir)/dat"

# export env
. $bin_dir/common-env

# init variables
email_from="uchadoop@dxy.cn"  
email_to="jiangh@dxy.cn,liuwx@dxy.cn"
email_subject="Doctor Influence Streaming Not Running"

# files
jar_file=$jar_dir/hbase-to-es-1.0-SNAPSHOT.jar
pid_file=$dat_dir/doctor_influence.pid
flag_file=$dat_dir/doctor_influence.flag
log_file=$log_dir/doctor_influence.log

# spark submit command
cmd='nohup spark-submit \
  --class com.dxy.data.app.DoctorInfluenceIndexStreaming \
  --executor-cores 1 \
  --executor-memory 4G \
  --driver-memory 2G \
  --num-executors 12 \
  --conf spark.streaming.backpressure.enabled=true \
  --conf spark.streaming.kafka.maxRatePerPartition=500 \
  --conf spark.streaming.backpressure.initialRate=10000 \
  --conf spark.streaming.stopGracefullyOnShutdown=true \
  --conf spark.locality.wait=0 \
  $jar_file PI-documents pi/checkpoint/doctor_influence 300 > $log_file 2>&1 & echo $! > $pid_file'


case "$1" in
    start)
        echo "ok" > $flag_file
        eval "$cmd"
        ;;
    stop)
        echo "wait" > $flag_file
        echo "Kill pid $(cat $pid_file)"
        kill $(cat $pid_file)
        ;;
    clean)
        echo "cleaning hdfs checkpoint dir ..."
        hadoop fs -rmr pi/checkpoint/doctor_influence
        ;;
    check)
        # check if pid exists
        # if error, res would be empty
        res=`ps h -fp $(cat $pid_file)`
        flag=`cat $flag_file`
        if [ "$res" == "" ] && [ "$flag" == "ok" ]; then
            echo "no doctor influence job running ..."
            /usr/bin/mail -s "$email_subject" $email_to < $log_file
            eval "$cmd"
        elif [ "$flag" == "wait" ]; then
            echo "wait for doctor influence to restart"
        else
            echo "doctor influence is running"
        fi
        ;;
    *)
        echo "usage: $0 (start|stop|clean|check)"
        exit 1
esac
