#!/bin/bash

if [ $# -lt 1 ]; then
  echo "usage: $0 (start|start_etl|stop|clean|check)"
  exit 1
fi

# init working dir
bin_dir=`dirname $0`
jar_dir="$(dirname $bin_dir)/jar"
log_dir="$(dirname $bin_dir)/log"
dat_dir="$(dirname $bin_dir)/dat"

# config env
. $bin_dir/common-env

# Batch Variables
# app name
user_action_app_name="PI-UserActionStreamForBatch"
user_info_app_name="PI-UserIdentityLinkingForBatch"
# topic names
topic_web_pv=PI-web_pv_user_log_batch
topic_app_pv=PI-app_pv_user_log_batch
topic_app_ev=PI-app_ev_user_log_batch
# files
jar_file=$jar_dir/user-profiling-sys-assembly-1.0.jar
# user info
log_file_user_info=$log_dir/user_info_batch.log
pid_file_user_info=$dat_dir/user_info_batch.pid
flag_file_user_info=$dat_dir/user_info_batch.flag
status_file_user_info="pi/batch/flag/user_info"
# user action
log_file_user_action=$log_dir/user_action_batch.log
pid_file_user_action=$dat_dir/user_action_batch.pid
flag_file_user_action=$dat_dir/user_action_batch.flag
status_file_user_action="pi/batch/flag/user_action"
# task log
log_file_task_log=$log_dir/pi_batch.log
# pid for batch
pid_file_app_server=$dat_dir/app_server_batch.pid
pid_file_app_da=$dat_dir/app_da_batch.pid
pid_file_web_pv=$dat_dir/web_pv_batch.pid

# Batch Schedule:
# - Delete all kafka topics.
# - Create 3 new kafka topics for batch job.
# - Put batch data into these topics.
# - Run Streaming Job until all data runs out.
# - Stop Streaming.

# - Run Streaming Job until all data runs out.
cmd_user_info='nohup spark-submit \
          --class UserInfoStreamingForBatch \
          --executor-cores 1 \
          --conf spark.streaming.backpressure.enabled=true \
          --conf spark.streaming.kafka.maxRatePerPartition=16000 \
          --conf spark.streaming.backpressure.initialRate=10000 \
          --driver-memory 2G \
          --executor-cores 1 \
          --executor-memory 2G \
          --num-executors 12 \
          --conf spark.streaming.stopGracefullyOnShutdown=true \
          --conf spark.locality.wait=0 \
          --conf spark.streaming.concurrentJobs=1 \
          --conf spark.scheduler.mode=FIFO \
          $jar_file $topic_web_pv $topic_app_pv $topic_app_ev > $log_file_user_info 2>&1 & echo $! > $pid_file_user_info'

cmd_user_action='nohup spark-submit \
          --class UserActionStreamingForBatch \
          --executor-cores 1 \
          --conf spark.streaming.backpressure.enabled=true \
          --conf spark.streaming.kafka.maxRatePerPartition=16000 \
          --conf spark.streaming.backpressure.initialRate=10000 \
          --driver-memory 2G \
          --executor-cores 1 \
          --executor-memory 2G \
          --num-executors 12 \
          --conf spark.streaming.stopGracefullyOnShutdown=true \
          --conf spark.locality.wait=0 \
          --conf spark.streaming.concurrentJobs=1 \
          --conf spark.scheduler.mode=FIFO \
          $jar_file $topic_web_pv $topic_app_pv> $log_file_user_action 2>&1 & echo $! > $pid_file_user_action'

case "$1" in
    start)
        echo "Starting user action and user info mission ..."
        # - Create 3 new kafka topics for batch job.
        kafka-topics --create --zookeeper master1.uc.host.dxy:2181 --replication-factor 2 --partitions 4 --topic $topic_web_pv
        kafka-topics --create --zookeeper master1.uc.host.dxy:2181 --replication-factor 2 --partitions 4 --topic $topic_app_pv
        kafka-topics --create --zookeeper master1.uc.host.dxy:2181 --replication-factor 2 --partitions 4 --topic $topic_app_ev
        # - Run Streaming Job until all data runs out.
        # spark submit command
        eval "$cmd_user_info"
        echo "running" > $flag_file_user_info
        eval "$cmd_user_action"
        echo "running" > $flag_file_user_action
        ;;
    start_etl)
        # check
        if [ $# -lt 3 ]; then
          echo "usage: $0 start_etl date0 date1"
          exit 1
        fi
        echo "Starting log reader mission ..."
        echo "task for $2 to $3" >> $log_file_task_log
        # - Put batch data into these topics.
        bash $bin_dir/web_pv.sh batch $topic_web_pv $2 $3
        bash $bin_dir/app_server.sh batch $topic_app_pv $2 $3
        bash $bin_dir/app_da.sh batch $topic_app_pv $topic_app_ev $2 $3
        ;;
    kill)
        # user info
        echo "Kill pid $(cat $pid_file_user_info) for user info .."
        kill -9 $(cat $pid_file_user_info)
        echo "stopped" > $flag_file_user_info
        # user action
        echo "Kill pid $(cat $pid_file_user_action) for user action .."
        kill -9 $(cat $pid_file_user_action)
        echo "stopped" > $flag_file_user_action
        # input application
        echo "Kill pid $(cat $pid_file_web_pv) for web pv .."
        kill $(cat $pid_file_web_pv)
        echo "Kill pid $(cat $pid_file_app_server) for app server .."
        kill $(cat $pid_file_app_server)
        echo "Kill pid $(cat $pid_file_app_da) for app da .."
        kill $(cat $pid_file_app_da)
        ;;
    clean)
        # - Delete all previous kafka topics.
        kafka-topics --delete --topic $topic_web_pv --if-exists --zookeeper master1.uc.host.dxy:2181 
        kafka-topics --delete --topic $topic_app_pv --if-exists --zookeeper master1.uc.host.dxy:2181
        kafka-topics --delete --topic $topic_app_ev --if-exists --zookeeper master1.uc.host.dxy:2181
        hadoop fs -rmr pi/batch
        ;;
    check)
        # user info
        user_info_flag=`cat $flag_file_user_info`
        user_info_res=`ps h -fp $(cat $pid_file_user_info)`
        if [ "$user_info_flag" == "running" ]; then
            if [ "$user_info_res" == "" ]; then
              echo "user info dead, restarting"
              /usr/bin/mail -s "user info job dead, restarting .." "jiangh@dxy.cn" < $log_file_user_info
              eval "$cmd_user_info"
            else
              hadoop fs -test -e $status_file_user_info
              if [ $? == 0 ]; then
                echo "user info job completed, kill from outside"
                appid=`$0 status | grep $user_info_app_name | awk '{print $1}'`
                echo "kill: $appid"
                yarn application -kill $appid
                echo "kill pid: `cat $pid_file_user_info`"
                kill `cat $pid_file_user_info`
                /usr/bin/mail -s "user info job completed" "jiangh@dxy.cn" < $log_file_task_log
                echo "stopped" > $flag_file_user_info
              else
                echo "user info is running"
              fi
            fi
        elif [ "$user_info_flag" == "stopped" ]; then
            echo "wait for user info to start next job"
        else
            echo "invalid user info flag, wtf? blackman with question marks ... "
        fi
        # user action
        user_action_flag=`cat $flag_file_user_action`
        user_action_res=`ps h -fp $(cat $pid_file_user_action)`
        if [ "$user_action_flag" == "running" ]; then
            if [ "$user_action_res" == "" ]; then
              echo "user action dead, restarting"
              /usr/bin/mail -s "user action job dead, restarting .." "jiangh@dxy.cn" < $log_file_user_action
              eval "$cmd_user_action"
            else
              hadoop fs -test -e $status_file_user_action
              if [ $? == 0 ]; then
                echo "user action job completed"
                appid=`$0 status | grep $user_action_app_name | awk '{print $1}'`
                echo "kill: $appid"
                yarn application -kill $appid
                echo "kill pid: `cat $pid_file_user_action`"
                kill `cat $pid_file_user_action`
                /usr/bin/mail -s "user action job completed" "jiangh@dxy.cn" < $log_file_task_log
                echo "stopped" > $flag_file_user_action
              else
                echo "user action is running"
              fi
            fi
        elif [ "$user_action_flag" == "stopped" ]; then
            echo "wait for user action to start next job"
        else
            echo "invalid user action flag, wtf is this?"
        fi
        ;;
    status)
        yarn application -list -appTypes spark -appStates ALL | awk '$2==appname && $4=="jiangh" && $6=="RUNNING"' appname=$user_action_app_name
        yarn application -list -appTypes spark -appStates ALL | awk '$2==appname && $4=="jiangh" && $6=="RUNNING"' appname=$user_info_app_name
        ;;
    start_ui)
        eval "$cmd_user_info"
        echo "running" > $flag_file_user_info
        ;;
    stop_ui)
        echo "stopped" > $flag_file_user_info
        echo "Kill pid $(cat $pid_file_user_info)"
        kill $(cat $pid_file_user_info)
        ;;
    start_ua)
        eval "$cmd_user_action"
        echo "running" > $flag_file_user_action
        ;;
    stop_ua)
        echo "stopped" > $flag_file_user_action
        echo "Kill pid $(cat $pid_file_user_action)"
        kill $(cat $pid_file_user_action)
        ;;
    *)
        echo "usage: $0 (start|start_etl|stop|clean|check)"
        exit 1
esac
