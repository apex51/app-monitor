#!/bin/bash

if [ $# -lt 1 ]; then
  echo "usage: $0 (start|stop|check)"
  exit 1
fi

# config env
export SPARK_HOME=/opt/cloudera/parcels/CDH/lib/spark
export PATH=$SPARK_HOME/bin:$PATH
export JAVA_HOME=/opt/java8
export PATH=/opt/java8/bin:$PATH

# init variables
email_from="uchadoop@dxy.cn"  
email_to="jiangh@dxy.cn"
email_subject="User Action Not Running"
email_content="User Action job is not running, trying to reboot yo ... "

# init working dir
bin_dir=`dirname $0`
jar_dir="$(dirname $bin_dir)/jar"
log_dir="$(dirname $bin_dir)/log"
dat_dir="$(dirname $bin_dir)/dat"

# files
jar_file=$jar_dir/user-profiling-sys-assembly-1.0.jar
pid_file=$dat_dir/user_action.pid
flag_file=$dat_dir/user_action.flag
log_file=$log_dir/user_action.log

# spark submit command
# cmd='nohup spark-submit --class UserActionStreaming --executor-cores 1 --num-executors 6 --conf spark.locality.wait=0 $jar_file > $log_file 2>&1 & echo $! > $pid_file'
# --conf spark.streaming.backpressure.enabled=true
# --conf spark.streaming.receiver.maxRate=80000
# --conf spark.streaming.backpressure.initialRate=400000

cmd='nohup spark-submit \
  --class UserActionStreaming \
  --conf spark.streaming.backpressure.enabled=true \
  --conf spark.streaming.kafka.maxRatePerPartition=500 \
  --conf spark.streaming.backpressure.initialRate=500 \
  --driver-memory 3G \
  --executor-cores 1 \
  --executor-memory 1G \
  --num-executors 12 \
  --conf spark.streaming.stopGracefullyOnShutdown=true \
  --conf spark.locality.wait=0 \
  --conf spark.streaming.concurrentJobs=1 \
  --conf spark.scheduler.mode=FIFO \
  $jar_file > $log_file 2>&1 & echo $! > $pid_file'


case "$1" in
    start)
        echo "ok" > $flag_file
        eval "$cmd"
        ;;
    stop)
        echo "wait" > $flag_file
        echo "Kill pid $(cat $pid_file)"
        kill $(cat $pid_file)
        ;;
    clean)
        echo "cleaning hdfs checkpoint dir ..."
        hadoop fs -rmr pi/checkpoint/user_action
        ;;
    check)
        # check if pid exists
        # if error, res would be empty
        res=`ps h -fp $(cat $pid_file)`
        flag=`cat $flag_file`
        if [ "$res" == "" ] && [ "$flag" == "ok" ]; then
            echo "no user action job running ..."
            /usr/bin/mail -s "$email_subject" $email_to < $log_file
            eval "$cmd"
        elif [ "$flag" == "wait" ]; then
            echo "wait for user action"
        else
            echo "user action is running"
        fi
        ;;
    *)
        echo "usage: $0 (start|stop|check)"
        exit 1
esac
