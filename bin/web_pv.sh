#!/bin/bash

if [ $# -lt 1 ]; then
  echo "usage: $0 (start|stop|check|batch)"
  exit 1
fi

# config env
export SPARK_HOME=/opt/cloudera/parcels/CDH/lib/spark
export PATH=$SPARK_HOME/bin:$PATH
export JAVA_HOME=/opt/java8
export PATH=/opt/java8/bin:$PATH

# init variables
email_from="uchadoop@dxy.cn"  
email_to="jiangh@dxy.cn"
email_subject="PI: Web PV Fast Parse Failed"
email_content="Web PV job is not running, trying to reboot yo ... "

# init working dir
bin_dir=`dirname $0`
jar_dir="$(dirname $bin_dir)/jar"
log_dir="$(dirname $bin_dir)/log"
dat_dir="$(dirname $bin_dir)/dat"

# files
external_files=$jar_dir/spark/url_regexes.yaml,$jar_dir/spark/mydata4vipday2.datx
jar_file=$jar_dir/WebLogFastParse-assembly-1.0.jar
pid_file=$dat_dir/web_pv.pid
flag_file=$dat_dir/web_pv.flag
log_file=$log_dir/web_pv.log

# spark submit command
cmd='nohup spark-submit \
  --class PVStreamAnalyzer \
  --driver-memory 2G \
  --executor-cores 1 \
  --executor-memory 1G \
  --num-executors 4 \
  --files $external_files --executor-cores 1 \
  --conf spark.streaming.stopGracefullyOnShutdown=true \
  --conf spark.streaming.kafka.maxRatePerPartition=5000 \
  --conf spark.streaming.backpressure.initialRate=5000 \
  --conf spark.locality.wait=0 \
  --conf spark.streaming.concurrentJobs=1 \
  --conf spark.scheduler.mode=FIFO \
  --executor-memory 1024M \
  --driver-memory 2048M \
  --conf spark.locality.wait=0 \
  $jar_file web-da-log etl/checkpoint tempdev.jiangh_bdl_web_pv_di tempdev.jiangh_bdl_web_pv_abnormal_di tempdev.jiangh_bdl_web_page_stay_seconds_di tempdev.jiangh_bdl_web_page_attrs_di 1 PI-web_pv_user_log_test > $log_file 2>&1 & echo $! > $pid_file'

# batch variables
pv_table=log.bdl_web_pv_di
attr_table=log.bdl_web_page_attrs_di
batch_log_file=$log_dir/web_pv_batch.log
batch_pid_file=$dat_dir/web_pv_batch.pid


case "$1" in
    start)
        echo "ok" > $flag_file
        eval "$cmd"
        ;;
    stop)
        echo "wait" > $flag_file
        echo "Kill pid $(cat $pid_file)"
        kill $(cat $pid_file)
        ;;
    check)
        # check if pid exists
        # if error, res would be empty
        res=`ps h -fp $(cat $pid_file)`
        flag=`cat $flag_file`
        if [ "$res" == "" ] && [ "$flag" == "ok" ]; then
            echo "no web pv job running ..."
            /usr/bin/mail -s "$email_subject" $email_to < $log_file
            eval "$cmd"
        elif [ "$flag" == "wait" ]; then
            echo "wait for web pv"
        else
            echo "web pv is running"
        fi
        ;;
    batch)
        echo "launch batch job for web pv"
        # Format: *.sh batch topic_name start_date end_date_optional
        if [ $# -eq 3 ]; then
            nohup spark-submit --class PVBatchAnalyzer --num-executors 10 --files $external_files $jar_file $pv_table $attr_table $2 $3 > $batch_log_file 2>&1 & echo $! > $batch_pid_file
        elif [ $# -eq 4 ]; then
            nohup spark-submit --class PVBatchAnalyzer --num-executors 10 --files $external_files $jar_file $pv_table $attr_table $2 $3 $4 > $batch_log_file 2>&1 & echo $! > $batch_pid_file
        else
            echo "usage: $0 batch topic_name start_date end_date_(optional)"
            exit 1
        fi
        ;;
    *)
        echo "usage: $0 (start|stop|check|batch)"
        exit 1
esac
