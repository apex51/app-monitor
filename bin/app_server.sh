#!/bin/bash

if [ $# -lt 1 ]; then
  echo "usage: $0 (start|stop|check|batch)"
  exit 1
fi

# config env
export SPARK_HOME=/opt/cloudera/parcels/CDH/lib/spark
export PATH=$SPARK_HOME/bin:$PATH
export JAVA_HOME=/opt/java8
export PATH=/opt/java8/bin:$PATH

# init variables
email_from="uchadoop@dxy.cn"  
email_to="jiangh@dxy.cn"
email_subject="PI: APP Server Fast Parse Failed"
email_content="APP Server job is not running, trying to reboot yo ... "

# init working dir
bin_dir=`dirname $0`
jar_dir="$(dirname $bin_dir)/jar"
log_dir="$(dirname $bin_dir)/log"
dat_dir="$(dirname $bin_dir)/dat"

# files
external_files=$jar_dir/spark/api_rules_app_server.yaml,$jar_dir/spark/mydata4vipday2.datx
jar_file=$jar_dir/AppServerFastParse-assembly-1.0.jar
pid_file=$dat_dir/app_server.pid
flag_file=$dat_dir/app_da.flag
log_file=$log_dir/app_server.log

# spark submit command
cmd='nohup spark-submit \
--class TaskStreaming \
--executor-cores 1 \
--num-executors 4 \
--executor-memory 1024M \
--driver-memory 1024M \
--conf spark.locality.wait=0 \
--files $external_files \
--conf spark.streaming.kafka.maxRatePerPartition=500 \
--conf spark.streaming.backpressure.initialRate=500 \
$jar_file tempdev.jiangh_bdl_log_app_server_di tempdev.jiangh_bdl_log_app_server_abnormal_di app-server-log etl/checkpoint 1 PI-app_pv_user_log_test > $log_file 2>&1 & echo $! > $pid_file'

# batch variables
server_table=log.bdl_app_server_di
batch_log_file=$log_dir/app_server_batch.log
batch_pid_file=$dat_dir/app_server_batch.pid


case "$1" in
    start)
        echo "ok" > $flag_file
        eval "$cmd"
        ;;
    stop)
        echo "wait" > $flag_file
        echo "Kill pid $(cat $pid_file)"
        kill $(cat $pid_file)
        ;;
    check)
        # check if pid exists
        # if error, res would be empty
        res=`ps h -fp $(cat $pid_file)`
        flag=`cat $flag_file`
        if [ "$res" == "" ] && [ "$flag" == "ok" ]; then
            echo "no app server job running ..."
            /usr/bin/mail -s "$email_subject" $email_to < $log_file
            eval "$cmd"
        elif [ "$flag" == "wait" ]; then
            echo "wait for app server"
        else
            echo "app server is running"
        fi
        ;;
    clean)
        echo "cleaning hdfs checkpoint dir ..."
        hadoop fs -rmr etl/checkpoint/app_server
        ;;
    batch)
        echo "launch batch job for app server"
        if [ $# -eq 3 ]; then
            nohup spark-submit --class TaskBatch --num-executors 10 --files $external_files $jar_file $server_table $2 $3 > $batch_log_file 2>&1 & echo $! > $batch_pid_file
        elif [ $# -eq 4 ]; then
            nohup spark-submit --class TaskBatch --num-executors 10 --files $external_files $jar_file $server_table $2 $3 $4 > $batch_log_file 2>&1 & echo $! > $batch_pid_file
        else
            echo "usage: $0 batch date1 (date2 optional)"
            exit 1
        fi
        ;;
    *)
        echo "usage: $0 (start|stop|check|batch)"
        exit 1
esac
